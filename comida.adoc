== ﻿Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Aperitivos

* Aceitunas
* Patatas fritas
* Queso
* Rebujina


=== Platos principales

* Chistorras
* Chuletas de cabeza
* Costillas de cerdo
* Paella
* Pancetas
* Salchichas
* Secreto ibérico


=== Postres

* Arroz con leche
* Flan
* Tarta de queso
